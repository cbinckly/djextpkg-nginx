######################################
expi - extender python package index
######################################

expi is a ``pip`` and ``PEP508`` compatible package index
for packages that are designed for use with `Extender`_.  It allows
developers to use `Python's standard packaging methods`__ and tools
to organize and distribute Extender customizations.

.. _Extender: https://www.orchid.systems/product/extender
.. __: https://packaging.python.org/

It has custom extensions, making marketing and distributing customizations
easy. To get started:

1. Register for a namespace.
2. Write your customization.
3. `Package it`_ along with a `special file`_, ``expi.json``.
4. Upload the package to your account.

.. _Package it: https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/
.. _special file: /package-format.html

A landing page will be generated automatically at 
``https://expi.2665093.ca/namespace/name`` and the package will be
installable using pip from any system connected to the internet.

``expi`` provides additional features over a public repository, 
like `pypi`_, intended to make it easier to sell and license Extender 
Customizations.

.. _pypi: https://pypi.org

- Package visibility and access can be controlled at a client by client level.
- Extra package metadata allows automatic version compatiblity checking.
- Auto-generated landing page customizable through the package.
- Installation and registration of custom module (``.vi``) files with Extender.

``expi`` is operated and maintained by `2665093 Ontario Inc`_.

.. _2665093 Ontario Inc: https://2665093.ca



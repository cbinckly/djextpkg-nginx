==================================
``expi`` Package Format Proposal
==================================

The ``expi`` package repository is intended primarily to store
and distribute packages for `Orchid Extender`_ scripts and modules.
This proposal is intended to enhance the repository to:

.. _Orchid Extender: https://www.orchid.systems/product/extender

- Allow Extender and Sage 300 Version and PU compatibility checking
- Enable the automatic generation of an enhanced landing page
  - Package Header
    - Author Information
    - Supported Sage and Extender versions
    - Licensing information
    - Pricing
  - Package Details
    - Contents of ``README.[md|rst]``
- Automatically import included module files
- Allow the execution of scripts or database changes post 
  package install.

Package Format
-----------------

The package format must be compatible with Python's native package
format.  To keep things simple, add a configuration file, ``expi.json``
to the package root::

    expi_skel/
            |_ setup.py
            |_ README.rst
            |_ LICENSE.txt
            |_ MANIFEST.in
            |_ docs/ # Sphinx/rst recommended
            |  |_ <package documentation>
            |_ expi_skel/
               |_ expi.json
               |_ __init__.py
               |_ <package files>
               |_ vi/
                  |_ EXPISKEL.company.vi

Check out the `expi_skel`_ package for a pre-built template with
example ``expi.json`` and ``setup.py`` files.

.. _expi_skel: htts://expi_skel.readthedocs.io

``expi.json``
-------------

The ``expi.json`` file has the following format::

    {
        "module": "CUSTMOD",
        "sage": {
            "version": 2019,
            "product_update": 3.0,
            "edition": "Premium",
        },
        "extender": 
            "version": 2019,
            "product_update": 7.1,
            "edition": "Developer",
        },
        "access": "public"|"private"|"authorized",
        "visibility": "public"|"private"|"authorized",
        "pricing": {
            "CAD": 100.00,
            "USD": 80.00,
            "AUD": 100.00,
            "EUR": 75.00,
        },
        "email": "sales_company.com",
        "postinstall": {
            "insert": {
                "table": "TABLE",
                "set": {
                    "FIELD": "VALUE",
                    "FIELD": "VALUE",
                    "FIELD": "VALUE"
                },
                "where": {
                    "FIELD": "VALUE"
                }
            },
            "update": {
                "table": "TABLE",
                "set": {
                    "FIELD": "VALUE",
                    "FIELD": "VALUE",
                    "FIELD": "VALUE"
                }
                "where": {
                    "FIELD": "VALUE"
                }
            },
            "run_scripts": ["scriptname", "scriptname"]
        }
    }

``vi/``
--------------

A special directory that may contain Extender module (``*.vi``) files.
Any files that exist in this directory will be imported into the 
Extender configuration.  It will be as though the module was imported
through the ``Modules`` screen, including custom tables, scripts, workflows,
and configuration.
